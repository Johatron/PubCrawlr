
import React from 'react'
import { connect } from 'react-redux'
import RouteCreator from '../components/RouteCreator/RouteCreator'
import { removePub, addPub, populatePubList } from '../actions/RouteCreatorActions'
import { addPubModal } from '../actions/MobileActions'

const mapStateToProps = state => {
	return {
		mobile: state.mobile,
		routeCreator: state.routeCreator,
		form: state.form
	}
}

const mapDispatchToProps = dispatch => {
	return {
		'addPub': pub => {
			dispatch(addPub(pub))
		},

		'removePub': index => {
			dispatch(removePub(index))
		},

		'populatePubList': pubList => {
			dispatch(populatePubList(pubList))
		},

		'addPubModal': add => {
			dispatch(addPubModal(add))
		}
	}
}

const RouteCreatorContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(RouteCreator)

export default RouteCreatorContainer
