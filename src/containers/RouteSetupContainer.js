
import React from 'react'
import { connect } from 'react-redux'
import RouteSetup from '../components/RouteSetup/RouteSetup'
import { addPubTitle } from '../actions/MobileActions'


const mapStateToProps = state => {
  return {
    routeSetup: state.routeSetup
  }
}

const mapDispatchToProps = dispatch => {
  return {
    submit: values => {
      dispatch(addPubTitle(values))
    }
  }
}

const RouteSetupContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RouteSetup)

export default RouteSetupContainer
