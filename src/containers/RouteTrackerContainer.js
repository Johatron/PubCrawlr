
import React from 'react'
import { connect } from 'react-redux'
import RouteTracker from '../components/RouteTracker/RouteTracker'
import { isMobile, removePub, addPub } from '../actions/RouteCreatorActions'

const mapStateToProps = state => {
	return {
		mobile: state.mobile,
		routeTracker: state.routeTracker
	}
}

const mapDispatchToProps = dispatch => {
	return {
		'addPub': pub => {
			dispatch(addPub(pub))
		},

		'removePub': index => {
			dispatch(removePub(index))
		},

		'isMobile': responsive => {
			dispatch(isMobile(responsive))
		},
	}
}

const RouteTrackerContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(RouteTracker)

export default RouteTrackerContainer