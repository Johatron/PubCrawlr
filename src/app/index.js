import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Reducers from '../reducers/reducers'
import App from './app'
import initalPubList from './data/pubs.json';

let store = createStore(Reducers,
	{
		mobile: {
			mobile: true,
			addPub: false,
			title: "Bristol Stout Crawl"
		},

		routeCreator: {

			mobile: false,

			routeMap: {
				zoom: 14,
				center: {
					lat: 51.454513,
					lng: -2.587910
				},
				pubList: initalPubList
			},

			controlPanel: {
				pubList: []
			}
		},

		routeTracker: {
			routeMap: {
				zoom: 14,
				center: {
					lat: 51.454513,
					lng: -2.587910
				},
				pubList: []
			}
		},

		routeSetup: {  }
	},
  applyMiddleware(thunk))


const isMobile = mobile => {
	store.dispatch({
		"type": "IS_MOBILE",
		mobile
	})
}

render (
    <Provider store={store}>
        <App isMobile={isMobile}/>
    </Provider>,
    document.getElementById('root')
)
