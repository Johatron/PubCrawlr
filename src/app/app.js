
import React from 'react'
import RouteCreatorContainer from '../containers/RouteCreatorContainer'
import RouteTrackerContainer from '../containers/RouteTrackerContainer'
import RouteSetupContainer from '../containers/RouteSetupContainer'
import { HashRouter as Router, Route } from 'react-router-dom'
import Grommet from 'grommet'
import Responsive from 'grommet/utils/Responsive'

export default class App extends React.Component {

    componentWillMount () {
        Responsive.start(responsive => (this.props.isMobile(responsive)));
    }

	render() {
		return (
			<Router>
				<Grommet.App centered={false}>
					<Route exact path="/" component={RouteSetupContainer} />
					<Route path="/routetracker" component={RouteTrackerContainer} />
					<Route path="/routecreator" component={RouteCreatorContainer} />
				</Grommet.App>
			</Router>
		)
	}
}
