
export const addPub = pub => ({ type: "ADD_PUB", pub })
export const removePub = index => ({ type: "REMOVE_PUB", index })
export const populatePubList = pubList =>({type: "POPULATE_PUB_LIST", pubList})
