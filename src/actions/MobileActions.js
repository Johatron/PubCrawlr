
const isMobile = mobile => ({type: 'IS_MOBILE', mobile})
export const addPubModal = addPub => ({type: 'ADD_PUB_MODAL', addPub})
export const addPubTitle = values => ({type: 'ADD_PUB_TITLE', values})
