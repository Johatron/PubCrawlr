
import update from 'immutability-helper'

const mobile = (state = {}, action) => {
	switch(action.type) {

		case "IS_MOBILE":
			return update(state, {
				mobile: { $set: action.mobile }
			})

		case "ADD_PUB_MODAL":
			return update(state, {
				addPub: { $set: action.addPub }
			})

		case "ADD_PUB_TITLE":
			console.log(action)
			return update(state, {
				title: { $set: action.values.title }
			})

		default:
			return state
	}
}

export default mobile
