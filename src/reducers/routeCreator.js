
import update from 'immutability-helper'

const routeCreator = (state = {}, action) => {
	switch(action.type) {

		case "ADD_PUB":
			return update(state, {
				controlPanel: {
					pubList: { $push: [action.pub] }
				}
			})

		case "REMOVE_PUB":
			return update(state, {
				controlPanel: {
					pubList: { $splice: [[action.index , 1]] }
				} 
			})

		default:
			return state
	}
}

export default routeCreator
