
import update from 'immutability-helper'

const routeTracker = (state = {}, action) => {
	switch(action.type) {

		case "POPULATE_PUB_LIST": 
			console.log("populate pub list")
			return update(state, {
				routeMap: {
					pubList: { $set: action.pubList }
				}
			})

		case "VISIT_PUB": 
			return update(state, {
				routeMap: {
					pubList: { $splice: [[action.index , 1]] }
				} 
			})

		default:
			return state
	}
}

export default routeTracker
