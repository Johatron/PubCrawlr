
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import routeSetup from './routeSetup'
import routeCreator from './routeCreator'
import routeTracker from './routeTracker'
import mobile from './mobile'

const Reducers = combineReducers ({
	form: formReducer,
	routeSetup,
	routeCreator,
	routeTracker,
	mobile
})

export default Reducers
