
import React from 'react'
import TextInput from 'grommet/components/TextInput'

const TextField = props =>
	<TextInput
		value={props.input.value}
		onDOMChange={param => props.input.onChange(param)}
		placeHolder={props.placeHolder}
		style={props.style}
	/>

export default TextField
