
import React from 'react'
import { Field, reduxForm } from 'redux-form'
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'
import Heading from 'grommet/components/Heading'
import Form from 'grommet/components/Form'
import { Route } from 'react-router-dom'

import TextField from './TextField'

const textStyle = {
	"color": "#040f16",
	"font-family": "'Montserrat', sans-serif",
	"background": "#FBFBFF",
}

let SetupForm = props => {

  const { handleSubmit } = props

  return (
    <Box justify="center" align="center" alignContent="center" >
			<Route render={({ history }) => (
				<Form onSubmit={ () => {handleSubmit(); history.push("/routecreator")} }  plain={true}>
					<Box>
						<Field name="title" component={TextField} placeHolder="Name your pub crawl" style={textStyle} />
						<Box size="medium" pad={{"vertical": "medium"}} style={{"font-family": "'Montserrat', sans-serif"}} align="end">
		    			<Button fill={true} primary={true} label={<Heading>Get crawling!</Heading>} type="submit" path="routecreator"onClick={handleSubmit} />
		    		</Box>
					</Box>
	  	  </Form>)}
			/>
    </Box>
  )
}

SetupForm = reduxForm({
  form: 'setupForm'
})(SetupForm)

export default SetupForm
