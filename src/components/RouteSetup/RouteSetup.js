
import React from 'react'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Header from 'grommet/components/Header'

import SetupForm from './SetupForm'

const style = {
	"background": "#040F16"
}

const RouteSetup = props =>
  <Box full={true} style={style} justify="center" align="center" alignContent="center">
		<Header>
			<Heading>
				<Box full={true} justify="center" align="center" alignContent="center">
					<SetupForm onSubmit={props.submit}/>
				</Box>
			</Heading>
		</Header>
  </Box>


export default RouteSetup
