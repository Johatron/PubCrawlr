
import React from 'react'
import Box from 'grommet/components/Box'
import Label from 'grommet/components/Label'
import Value from 'grommet/components/Value'
import StarRatingComponent from 'react-star-rating-component'

const style = {
	"font-family": "Montserrat",
	"font-size": "0.8em",
	"color": "#040F16"
}

const textStyle = {
	"color": "#040F16"
}

const CardPriceRating = ({pubPriceRating}) =>

	<Box pad={{"horizontal": "small"}} style={style}>
		<Label align="center" margin="small">
			Price
		</Label>

        <Box>
			<Value value={pubPriceRating} units='/10' size='xsmall' style={textStyle}/>
        </Box>
        <Box direction="row" justify="center" style={{fontSize: 20}}  align="center" >
	        <StarRatingComponent
	            name="app5"
	            starColor="#85bb65"
	            emptyStarColor="#434343"
	            value={Math.round(pubPriceRating)/2}
	            renderStarIcon={(index, value) => 
	            	<span style={{padding: "27%"}} className={index <= value ? 'fa fa-gbp' : 'fa fa-gbp'} />
	            }
	            editing={false}
	        />
        </Box>
	</Box>

export default CardPriceRating