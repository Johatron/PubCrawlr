
import React from 'react'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Header from 'grommet/components/Header'
import Form from 'grommet/components/Form'
import Label from 'grommet/components/Label'
import Value from 'grommet/components/Value'
import Button from 'grommet/components/Button'
import StarRatingComponent from 'react-star-rating-component'
import { Route } from 'react-router-dom'
import { Field, reduxForm } from 'redux-form'

import TextField from '../RouteSetup/TextField'

const style = {
    "font-family": "Montserrat",
    "color": "#040F16"
}

const GeocodeInput = (pubName, addresLine1, addresLine2, city, postcode, addPub) => {
  let address = addresLine1 + ", " + addresLine2 + ", " + city + ", " + postcode
  console.log(address)
  let geocoder = new google.maps.Geocoder();
  geocoder.geocode({"address": address}, (results, status) => {
    if (status === google.maps.GeocoderStatus.OK) {
      let pub = {
        "addressLineOne": addresLine1,
        "addressLineTwo": addresLine2,
        "pubName": pubName,
        "position": {
          "lat": results[0].geometry.location.lat(),
          "lng": results[0].geometry.location.lng()},
        "pubPriceRating": 1.6,
        "pubStarRating": 3.2}
      addPub(pub)
    }
  })
}

let AddPub = props => {

  console.log(props.form["object Object"])

  const { handleSubmit } = props

  return (
        <Box align="center" size="xlarge" pad="small">
            <Form plain={true} onSubmit={() => console.log('test')} >
                <Box direction="row" justify="center" >
                    <Box justify="between">
                        <Box>
                            <Heading tag="h3" style={style}> Pub Name: </Heading>
        					           <Box pad="small"><Field name="pubName" component={TextField} type="text" placeHolder="Pub name" style={style} /></Box>
                        </Box>

                        <Box>
                            <Heading tag="h3" style={style}> Pub Rating: </Heading>
                            <Box direction="row" justify="between">
                                <Box pad="small" justify="center" style={{fontSize: 20}} >
                                    <Heading tag="h4" style={style}> Rating </Heading>
                                    <StarRatingComponent
                                        name="starrater"
                                        starColor="#ffb400"
                                        emptyStarColor="#ffb400"
                                        renderStarIcon={(index, value) =>
                                            <span style={{padding: "15%"}} className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />
                                        }
                                        renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
                                    />
                                </Box>
                                <Box pad="small" justify="center" style={{fontSize: 20}} >
                                    <Heading tag="h4" style={style}> Price </Heading>
                                    <StarRatingComponent
                                        name="pricerater"
                                        starColor="#85bb65"
                                        emptyStarColor="#434343"
                                        renderStarIcon={(index, value) =>
                                            <span style={{padding: "27%"}} className={index <= value ? 'fa fa-gbp' : 'fa fa-gbp'} />
                                        }
                                    />
                                </Box>
                            </Box>
                        </Box>
                    </Box>

                    <Box>
                        <Heading tag="h3" style={style}> Pub Address: </Heading>
  					            <Box pad="small"><Field name="addresLine1" component={TextField} type="text" placeHolder="Address Line 1" style={style} /></Box>
   					            <Box pad="small"><Field name="addresLine2" component={TextField} type="text" placeHolder="Address Line 2" style={style} /></Box>
   					            <Box pad="small"><Field name="city" component={TextField} type="text" placeHolder="City" style={style} /></Box>
   					            <Box pad="small"><Field name="postcode" component={TextField} type="text" placeHolder="Postcode" style={style} /></Box>
                    </Box>
                </Box>

                <br />
                <br />
                <br />

                <Box direction="row" justify="end" align="end" alignContent="end" alignSelf="end">
                    <Box pad="small" size="small">
                        <Button critical={true} onClick={() => props.addPubModal(false)} fill={true} label="Cancel"/>
                    </Box>
                    <Box pad="small" size="small">
                        <Button onClick={() => GeocodeInput(props.form["object Object"].values.pubName,
                                                            props.form["object Object"].values.addresLine1,
                                                            props.form["object Object"].values.addresLine2,
                                                            props.form["object Object"].values.city,
                                                            props.form["object Object"].values.postcode,
                                                            props.addPubFunc)} fill={true} label="Save"/>
                    </Box>
                </Box>
            </Form>
        </Box>
  )
}

AddPub = reduxForm({
  form: 'addPub'
})(AddPub)

export default AddPub
