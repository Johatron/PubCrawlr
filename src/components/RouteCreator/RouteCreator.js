
import React from 'react'
import Split from 'grommet/components/Split'
import Box from 'grommet/components/Box'
import Toast from 'grommet/components/Toast'
import LocationIcon from 'grommet/components/icons/base/Location'
import Heading from 'grommet/components/Heading'
import Header from 'grommet/components/Header'
import Layer from 'grommet/components/Layer'
import { Scrollbars } from 'react-custom-scrollbars'
import 'grommet/scss/hpe/index'

import PubList from './PubList'
import RouteMap from './RouteMap'
import RouteTitle from './RouteTitle'
import RouteMonitor from './RouteMonitor'
import ControlPanelButtons from './ControlPanelButtons'
import AddPub from './AddPub'

const style = {
    "font-family": "Montserrat",
    "color": "#040F16"
}

const RouteCreator = props => {
    if(props.mobile.mobile) {
        return (
            <Box>
                <RouteTitle title={props.mobile.title} />
                <Box size={{height: "medium"}} pad="small">
                    <RouteMap mobile={props.mobile.mobile} routeMap={props.routeCreator.routeMap} controlPanel={props.routeCreator.controlPanel} onMarkerClick={props.addPub}/>
                </Box>
                <Box align="center">
                    <PubList mobile={props.mobile.mobile} controlPanel={props.routeCreator.controlPanel} removePub={props.removePub} populatePubList={props.populatePubList} />
                    <hr/>
                    <RouteMonitor pubList={props.routeCreator.controlPanel.pubList} />
                    <ControlPanelButtons addPubModal={props.addPubModal} pubList={props.routeCreator.controlPanel.pubList} populatePubList={props.populatePubList} />
                </Box>

                <Layer hidden={props.mobile.addPub ? false : true} align={props.mobile.mobile ? "right": "center"} onClose={() => props.addPubModal(false)} closer={true}>
                    <Header>
                        <Heading tag="h2" style={style}>
                            Add a custom pub.
                        </Heading>
                    </Header>
                      <AddPub addPubModal={props.addPubModal} mobile={props.mobile} />
                </Layer>
            </Box>
        )
    } else {
        return (
            <Box flex={false} full={"vertical"}>
                <Toast>
                  Click on the red location icons to add a pub to the route. <LocationIcon style={{"color": "#DC143C"}} />
                </Toast>
                <RouteTitle title={props.mobile.title} />
                    <Box direction="row" flex={true}>
                      <Box size={{width: "medium"}}>
                          <PubList controlPanel={props.routeCreator.controlPanel} removePub={props.removePub} populatePubList={props.populatePubList} />
                          <Box size={{width: "medium"}} ><hr/></Box>
                          <RouteMonitor pubList={props.routeCreator.controlPanel.pubList} />
                          <ControlPanelButtons addPubModal={props.addPubModal} pubList={props.routeCreator.controlPanel.pubList} populatePubList={props.populatePubList} />
                      </Box>
                      <Box flex={true}>
                          <RouteMap mobile={props.mobile.mobile} routeMap={props.routeCreator.routeMap} controlPanel={props.routeCreator.controlPanel} onMarkerClick={props.addPub}/>
                      </Box>
                  </Box>

                  <Layer hidden={props.mobile.addPub ? false : true} align={props.mobile.mobile ? "right": "center"} onClose={() => props.addPubModal(false)} closer={true}>
                      <Header>
                          <Heading tag="h2" style={style}>
                              Add a custom pub.
                          </Heading>
                      </Header>
                      <AddPub addPubModal={props.addPubModal} mobile={props.mobile} form={props.form} addPubFunc={props.addPub} />
                  </Layer>
            </Box>
        )
    }
}

export default RouteCreator
