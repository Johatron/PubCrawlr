
import React from 'react'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import Header from 'grommet/components/Header'

const style = {
	"background": "#040F16"
}

const textStyle = {
	"font-family": "'Montserrat', sans-serif",
	"color": "#FBFBFF",
}

const RouteTitle = ({title}) =>
	<Box full="horizontal" pad="small" align="center" style={style}>
		<Heading>
			<Header tag="h6" style={textStyle}>
				{title}
			</Header>
		</Heading>
	</Box>

export default RouteTitle
