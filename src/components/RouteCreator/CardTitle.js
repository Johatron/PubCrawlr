
import React from 'react'
import Box from 'grommet/components/Box'
import Heading from 'grommet/components/Heading'
import FontAwesome from 'react-fontawesome'

const style = {
	"fontFamily": "'Montserrat', sans-serif",
	"font-size": "1.3em",
	"fontWeight": 400,
	"color": "#FBFBFF",
	"text-rendering": "optimizeLegibility",
    "text-rendering": "geometricPrecision",
    "font-smooth": "always",
    "font-smoothing": "antialiased",
    "-moz-font-smoothing": "antialiased"
}

const CardTitle = ({pubName}) =>
	<Box responsive={false} pad={{"vertical": "small", "horizontal": "small"}} direction="row">
		<FontAwesome name='beer' style={{"color": "#FBFBFF"}} size='2x' />
		<Box pad={{"horizontal": "small"}}>
			<Heading tag="h2" margin="none" style={style}>
				{pubName}
			</Heading>
		</Box>
	</Box>

export default CardTitle