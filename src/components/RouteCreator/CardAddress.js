
import React from 'react'
import Box from 'grommet/components/Box'
import Paragraph from 'grommet/components/Paragraph'
import LocationIcon from 'grommet/components/icons/base/Location';

// #0b4f6c - Dark blue
// #01baef - Cyan


const style = {
	"font-family": "Merriweather",
	"font-size": "1.1em",
	"color": "#040F16"	
}

const CardAddress = ({addressLineOne, addressLineTwo}) => 
	<Box responsive={false} direction="row" pad="small">
		<Box pad={{"horiztonal": "small"}}>
			<LocationIcon size="small" colorIndex="critical" />
		</Box>
		<Box pad={{"horizontal": "small"}}  >
			<Paragraph size="large" margin="none" style={style}> {addressLineOne} </Paragraph>
			<Paragraph size="large" margin="none" style={style}> {addressLineTwo} </Paragraph>
		</Box>
	</Box>

export default CardAddress