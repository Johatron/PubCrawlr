
import React from 'react'
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'

const ControlPanelButtons = ({addPubModal, populatePubList, pubList}) =>
	<Box size={{width: "medium"}} >
		<Box pad="small" direction="row" justify="center" align="center" alignContent="center" >
			<Box pad="small" size="medium" justify="center" align="center" alignContent="center" >
				<Button onClick={() => addPubModal(true)} label="Add Pub" fill={true} />
			</Box>
			<Box pad="small" size="medium" justify="center" align="center" alignContent="center" >
				<Button primary={true} onClick={() => populatePubList(pubList)} label="Save Route" fill={true} path={"/routetracker"} />
			</Box>
		</Box>
	</Box>

export default ControlPanelButtons