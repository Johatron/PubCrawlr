
import React from 'react'
import Box from 'grommet/components/Box'
import Meter from 'grommet/components/Meter'
import Value from 'grommet/components/Value'

const RouteMeter = ({max, units, value}) =>
	<Box>
		<Meter size='xsmall' type='circle' max={max} value={value} colorIndex={colourCalulator(max, value)} />
		<Value value={value} units={units} size='xsmall' />
	</Box>

const colourCalulator = (max, value) => {
	if(value/max < 1/3) {
		return "ok"
	} else if (value/max >= 1/3 && value/max <= 2/3) {
		return "warning"
	} else {
		return "critical"
	}
}

export default RouteMeter