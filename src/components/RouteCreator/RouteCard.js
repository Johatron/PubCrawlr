
import React from 'react'
import Sidebar from 'grommet/components/Sidebar'
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'
import CloseIcon from 'grommet/components/icons/base/Close'


import CardTitle from './CardTitle'
import CardAddress from './CardAddress'
import CardPriceRating from './CardPriceRating'
import CardStarRating from './CardStarRating'

const style = {
	"box-shadow": "2px 2px 1px #888888",
	"border-radius": "3px"
}

const titleStyle = {
	"background": "#040F16",
	"border-top-left-radius": "3px",
	"border-top-right-radius": "3px",
}

const RouteCard = ({mobile, pub, index, removePub}) => 
	<Box pad={{"vertical": "small", "horizontal": "small"}}>
		<Box colorIndex="light-1" style={style}>
			
			<Box responsive={false} direction="row" justify="between" style={titleStyle} >
				<CardTitle pubName={pub.pubName}/>
				<Button box={true} pad={"none"} fill={false} icon={<CloseIcon size="small" colorIndex="light-1" />} onClick={() => removePub(index)} />
			</Box>
			<CardAddress addressLineOne={pub.addressLineOne} addressLineTwo={pub.addressLineTwo} />

			<Box pad={{"horizontal": "small"}} direction="row" justify="between" responsive={false} >
				<CardPriceRating pubPriceRating={pub.pubPriceRating} />
				<CardStarRating pubStarRating={pub.pubStarRating} />
			</Box>
		</Box>
	</Box>

export default RouteCard