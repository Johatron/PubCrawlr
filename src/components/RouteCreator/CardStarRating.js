
import React from 'react'
import Box from 'grommet/components/Box'
import Label from 'grommet/components/Label'
import Value from 'grommet/components/Value'
import StarRatingComponent from 'react-star-rating-component'

const style = {
	"font-family": "Montserrat",
	"font-size": "0.8em",
	"color": "#040F16"
}

const textStyle = {
	"color": "#040F16"
}

const CardStarRating = ({pubStarRating}) =>

	<Box pad={{"horizontal": "small"}} style={style} >
		<Label align="center" margin="small">
			Rating
		</Label>

        <Box>
			<Value value={pubStarRating} units='/10' size='xsmall' style={textStyle}/>
        </Box>
        <Box direction="row" justify="center" style={{fontSize: 20}}  align="center" >
	        <StarRatingComponent
	            name="app5"
	            starColor="#ffb400"
	            emptyStarColor="#ffb400"
	            value={Math.round(pubStarRating/2)}
	            renderStarIcon={(index, value) => 
	            	<span style={{padding: "15%"}} className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />
	            }
	            editing={false}
	            renderStarIconHalf={() => <span className="fa fa-star-half-full" />}
	        />
        </Box>
	</Box>

export default CardStarRating