
import React from 'react'
import Box from 'grommet/components/Box'
import Meter from 'grommet/components/Meter'
import Value from 'grommet/components/Value'
import Button from 'grommet/components/Button'
import { Scrollbars } from 'react-custom-scrollbars'

import RouteTitle from './RouteTitle'
import RouteCard from './RouteCard'
import RouteMeter from './RouteMeter'

import Background from '../../../img/skulls.png'
const style = {
    "backgroundImage": `url(${Background})`,
    "backgroundRepeat": "repeat",
    "backgroundSize": "auto, cover"
}

const PubList = ({mobile, controlPanel, removePub, populatePubList}) => {
	if(mobile) {
		return (
			<Box full={"horizontal"} style={style}>
				{controlPanel.pubList.map( (pub, index) => 
					<RouteCard
						mobile={mobile}
						key={index}
						pub={pub}
						index={index}
						removePub={removePub}
					/>
				)}
			</Box>
		)
	} else {
		return (
			<Box flex={true} size={{width: "medium"}} style={style}>
				<Scrollbars>
					{controlPanel.pubList.map( (pub, index) => 
						<RouteCard
							mobile={mobile}
							key={index}
							pub={pub}
							index={index}
							removePub={removePub}
						/>
					)}
				</Scrollbars>
			</Box>
		)
	}
}

export default PubList

