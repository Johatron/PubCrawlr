
import React from 'react'
import Box from 'grommet/components/Box'
import { withGoogleMap, GoogleMap, Marker, Polyline } from "react-google-maps"
import SnowStorm from 'react-snowstorm'

const RouteMap = ({mobile, routeMap, controlPanel, onMarkerClick}) => 
	<MainMap
		containerElement={
	  		<div style={{ height: '100%', width: '100%' }} />
		}

		mapElement={
	  		<div style={{ height: '100%', width: '100%' }} />
		}

		routeMap={routeMap}
		controlPanel={controlPanel}
		onMarkerClick={onMarkerClick}
	/>

const MainMap = withGoogleMap(({routeMap, controlPanel, onMarkerClick}) => (
 	<GoogleMap defaultZoom={routeMap.zoom} defaultCenter={routeMap.center} options={{gestureHandling: "cooperative", optimized: false}}>
 		{routeMap.pubList.map( (pub, index) => 
 			<Marker
 				options={{optimized: false}}
 				optimized={false}
 				key={index}
 				{...pub}
 				onClick={() => onMarkerClick(pub)}
 			/>
 		)}
 		{controlPanel.pubList.map( (pub, index) => 
 			<Polyline
 				key={index}
 				path={controlPanel.pubList.map(pub => pub.position)}
 			/>
 		)}
 	</GoogleMap>
));

export default RouteMap
