
import React from 'react'
import Box from 'grommet/components/Box'
import geolib from 'geolib'

import RouteMeter from './RouteMeter'

const RouteMonitor = ({pubList}) =>
    <Box size={{width: "medium"}} responsive={false} pad={{"horizontal": "small"}} direction="row" justify="between">
        <RouteMeter max={12} units='pubs' value={Number(pubList.length)} united='' />
        <RouteMeter max={5000} units='meters' value={Number(geolib.getPathLength(pubList
                    .map(pub => { return {latitude: pub.position.lat, longitude: pub.position.lng}})
                ))} />
        <RouteMeter max={100} units='£' value={Number(pubList.map(pub => pub.pubPriceRating).reduce((a, b) => a + b, 0).toFixed(2))}/>
    </Box>

export default RouteMonitor