
import React from 'react'
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'

const ControlPanelButtons = ({populatePubList, pubList}) =>
	<Box size="large"  justify="center" align="center" alignContent="center" >
		<Box pad="small" size="medium" justify="center" align="center" alignContent="center" >
			<Button onClick={() => console.log("test")} label="Visit Pub" fill={true} path={"/routetracker"} />
		</Box>
	</Box>

export default ControlPanelButtons