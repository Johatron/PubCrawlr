
import React from 'react'
import Split from 'grommet/components/Split'
import Box from 'grommet/components/Box'
import Toast from 'grommet/components/Toast'
import LocationIcon from 'grommet/components/icons/base/Location'
import Heading from 'grommet/components/Heading'
import { Scrollbars } from 'react-custom-scrollbars'
import 'grommet/scss/hpe/index'

import PubList from '../RouteCreator/PubList'
import RouteMap from '../RouteCreator/RouteMap'
import RouteTitle from '../RouteCreator/RouteTitle'
import RouteMonitor from '../RouteCreator/RouteMonitor'
import ControlPanelButtons from './ControlPanelButtons'

const RouteTracker = props => {
    if(props.mobile.mobile) {
        return (
            <Box>
                <RouteTitle title={props.mobile.title}/>
                <Box size={{height: "medium"}} pad="small">
                    <RouteMap mobile={props.mobile.mobile} height="100%" routeMap={props.routeTracker.routeMap} controlPanel={props.routeTracker.routeMap} onMarkerClick={props.addPub}/>
                </Box>
                <Box align="center">
                    <PubList mobile={props.mobile.mobile} controlPanel={props.routeTracker.routeMap} removePub={props.removePub} populatePubList={props.populatePubList} />
                    <hr/>
                    <RouteMonitor pubList={props.routeTracker.routeMap.pubList} />
                    <ControlPanelButtons pubList={props.routeTracker.routeMap.pubList} populatePubList={props.populatePubList} />
                </Box>
            </Box>
        )
    } else {
        return (
            <Box flex={false} full={"vertical"}>
                <RouteTitle  title="test" />
                    <Box direction="row" flex={true}>
                        <Box size={{width: "medium"}}>
                            <PubList controlPanel={props.routeTracker.routeMap} removePub={props.removePub} populatePubList={props.populatePubList} />
                            <hr/>
                            <RouteMonitor pubList={props.routeTracker.routeMap.pubList} />
                            <ControlPanelButtons pubList={props.routeTracker.routeMap.pubList} populatePubList={props.populatePubList} />
                        </Box>
                    <Box flex={true}>
                        <RouteMap mobile={props.mobile.mobile} height="100vh" routeMap={props.routeTracker.routeMap} controlPanel={props.routeTracker.routeMap} onMarkerClick={props.addPub}/>
                    </Box>
                </Box>
            </Box>
        )
    }
}

export default RouteTracker
