
import pandas as pd
import random
import json

headings = ["number", "name", "address", "postcode", "f", "g", "lat", "lng", "city"]

pubFile = pd.read_csv("./open_pubs.csv", names=headings)
bristolPubs = pubFile.loc[pubFile["city"] == "Bristol, City of"]

bristolPubsList = []
for index, row in bristolPubs.iterrows():
    bristolPubsList.append({
        "pubName": row["name"],
        "pubPriceRating": round(random.random()*10,1),
        "pubStarRating": round(random.random()*10,1),
        "addressLineOne": row["address"].split(",")[0],
        "addressLineTwo": row["address"].split(",")[1],
        "position": {
            "lat": float(row["lat"]),
            "lng": float(row["lng"])
        }
    })

with open('../src/app/data/pubs.json', 'w') as outfile:
    json.dump(bristolPubsList, outfile)

