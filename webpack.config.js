
var webpack = require("webpack");
var sass = require('node-sass');
var jsonImporter = require('node-sass-json-importer');
var path = require('path');

module.exports = {
    entry: __dirname+'/src/app/',

    output: {
        path: __dirname+'/public/javascript',
        filename: 'bundle.js'
    },

    devServer: {
        inline: true,
        contentBase: __dirname+'/public',
        port: 8100
    },

    resolve: {
		extensions: ['*', '.js', '.scss', '.css']
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            },

            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded&' +'includePaths[]=' +(encodeURIComponent(path.resolve(process.cwd(), './node_modules'))) +'&includePaths[]=' +(encodeURIComponent(path.resolve( process.cwd(),'./node_modules/grommet/node_modules')))
            },

            {
                test: /\/json$/,
                loader: 'json-loader'
            },

            {
                test: /\.(?:png|jpg|svg)$/,
                loader: 'url-loader',
                query: {
                    limit: 1000000
                }
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),

	new webpack.LoaderOptionsPlugin({
    		options: {
      			sassLoader: {
        			importer: jsonImporter,
      			},
      		context: __dirname,
    		},
  	})
    ]
}

